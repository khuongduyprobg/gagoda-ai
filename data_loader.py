### Module imports ###
import torch
import torchvision
import torchvision.transforms as transforms
from torchvision.datasets import ImageFolder
from torch.utils.data import DataLoader

import os


### Global Variable ###
transforms_dict = {
    'train': transforms.Compose(
        [transforms.Resize([256, 256]),
         transforms.RandomCrop(224),
         transforms.RandomHorizontalFlip(),
         transforms.ToTensor(),
         transforms.Normalize(mean=[0.485, 0.456, 0.406],
                              std=[0.229, 0.224, 0.225])]),
    'val': transforms.Compose(
        [transforms.Resize([224, 224]),
         transforms.ToTensor(),
         transforms.Normalize(mean=[0.485, 0.456, 0.406],
                              std=[0.229, 0.224, 0.225])])
}
root_folder = 'data'


### Class declarations ###


### Function declarations ###
def load_data(root_folder, mode='train', batch_size=64):
    """
    Load image data from folder
    Input: root_folder: root folder contain train/val set
           mode: 'train' or 'val'
           batch_size: batch_size of dataloader
    Output: data_loader
    """
    folder_path = os.path.join(root_folder, mode)
    data = ImageFolder(folder_path, transform=transforms_dict[mode])
    dataloader = DataLoader(data, batch_size=batch_size, shuffle=True)
    return dataloader


if __name__ == '__main__':
    # Test
    train_loader = load_data(root_folder, mode='train', batch_size=64)
    val_loader = load_data(root_folder, mode='val', batch_size=64)
    for X_batch, y_batch in train_loader:
        print(X_batch.shape, y_batch.shape)
    # print(len(train_loader))
    # dataiter = iter(train_loader)
    # images, labels = next(dataiter)
    # print(images.shape, labels.shape)
