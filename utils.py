### Module imports ###
import torch
from torch.utils.data import Dataset, DataLoader
import torchvision.transforms as transforms

from PIL import Image
import numpy as np
import pandas as pd


### Global Variable ###
transforms_dict = {
    'train': transforms.Compose(
        [transforms.Resize([224, 224]),
         transforms.RandomCrop(224),
         transforms.RandomHorizontalFlip(),
         transforms.ToTensor(),
         transforms.Normalize(mean=[0.485, 0.456, 0.406],                                                   std=[0.229, 0.224, 0.225])]),
    'val': transforms.Compose(
        [transforms.Resize([224, 224]),
         transforms.ToTensor(),
         transforms.Normalize(mean=[0.485, 0.456, 0.406],
                             std=[0.229, 0.224, 0.225])])
}


### Class declarations ###
class FurnitureDataset(Dataset):
    """ Furniture dataset """
    def __init__(self, csv_file, transforms):
        self.df = pd.read_csv(csv_file)
        self.transforms = transforms

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        img_name = self.df.iloc[idx, 0]
        label = self.df.iloc[idx , 1]

        image = Image.open(img_name)
        image = np.asarray(image)
        image = Image.fromarray(image)
        image = self.transforms(image)

        label = np.asarray(label)
        label = torch.from_numpy(label).long()

        return (image, label)


### Function declarations ###
if __name__ == '__main__':
    dataset = FurnitureDataset('data/train.csv', transforms=transforms_dict['train'])
    train_loader = DataLoader(dataset, batch_size=64, shuffle=True)
    for X_batch, y_batch in train_loader:
        print(X_batch.shape, y_batch.shape)
