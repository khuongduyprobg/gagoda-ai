### Module imports ###
import os
import numpy as np
import pandas as pd
import pickle
from PIL import Image


### Global Variable ###
root_folder = 'data'


### Class declarations ###


### Function declarations ###
def check_rgb(img_path):
    """
    Check if image is rgb image and ignore grayscale image
    Input: image path
    Output: True if rgb image else False
    """
    image = Image.open(img_path)
    image = np.asarray(image)
    if len(image.shape) == 2:
        return False
    return True


def create_dict(root_folder):
    """
    Create classes dictionary from data folder
    Input: Datafolder path contains classes folder
    Output: Classes dictionary
    """
    data_folder = os.path.join(root_folder, 'train')
    class_folder = os.listdir(data_folder)
    class_dict = dict()
    for i in range(len(class_folder)):
        class_dict[class_folder[i]] = i

    with open(os.path.join(root_folder, 'dict.pkl'), 'wb') as f:
        pickle.dump(class_dict, f)
    return class_dict


def create_csv(root_folder, class_dict, mode='train'):
    """
    Create csv file for train/test set
    Input: root_folder: original folder of data
           class_dict: Classes dictinary
           mode: train/val
    Output: Create csv file
    """
    img_path, labels = [], []
    data_folder = os.path.join(root_folder, mode)
    classes = os.listdir(data_folder)
    for label in classes:
        images_path = os.path.join(data_folder, label)
        for fn in os.listdir(images_path):
            img_name = os.path.join(images_path, fn)
            if check_rgb(img_name):
                img_path.append(img_name)
                labels.append(label)

    df = pd.DataFrame(columns=['img', 'label'])
    df['img'] = img_path
    df['label'] = labels
    df['label'] = df['label'].map(class_dict)
    output_path = os.path.join(root_folder, mode + '.csv')
    df.to_csv(output_path, index=False)


if __name__ == '__main__':
    class_dict = create_dict(root_folder)
    print(class_dict)
    create_csv(root_folder, class_dict, mode='train')
    create_csv(root_folder, class_dict, mode='val')
