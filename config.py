# Data
train_csv = 'data/train.csv'
val_csv = 'data/val.csv'
class_path = 'data/dict.pkl'

# Model
model_path = 'model/best_model.pkl'
