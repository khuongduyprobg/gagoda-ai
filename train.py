### Module imports ###
import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
import torchvision
from torchvision import datasets, models, transforms
from torch.utils.data import DataLoader

import numpy as np
import os
from fastprogress import master_bar, progress_bar

import config as cf
from utils import *


### Global Variable ###
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


### Class declarations ###


### Function declarations ###
def train_model(model, criterion, optimizer, scheduler, train_loader, val_loader, num_epochs=100):
    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
    model = model.to(device)
    mb = master_bar(range(num_epochs))
    performance = 0

    for epoch in mb:
        model.train()
        print(f'Epoch {epoch + 1}/{num_epochs}')

        for inputs, labels in progress_bar(train_loader):
            inputs = inputs.to(device)
            labels = labels.to(device)

            model.zero_grad()
            optimizer.zero_grad()

            predictions = model(inputs)
            predictions = predictions.to(device)

            loss = criterion(predictions, labels)
            loss.backward()
            optimizer.step()

        with torch.no_grad():
            correct, label = 0, 0
            for inputs, labels in val_loader:
                inputs = inputs.to(device)
                labels = inputs.to(device)
                outputs = model(inputs)
                _, predictions = torch.max(outputs.data, 1)

                total += label.size(0)
                correct += (predictions == labels).item().sum()
                acc = correct / total
                print(f"Accuracy: {acc}")

                if acc > performance:
                    performance = acc
                    if not os.path.exists('model'):
                        os.makedirs('model')
                    torch.save(model.state_dict(), cf.model_path)
                print(f"Best Accuracy: {performance}")
                print("-" * 10)


if __name__ == '__main__':
    traindata = FurnitureDataset(cf.train_csv, transforms_dict['train'])
    valdata = FurnitureDataset(cf.val_csv, transforms_dict['val'])
    train_loader = DataLoader(traindata, batch_size=2, shuffle=True)
    val_loader = DataLoader(valdata, batch_size=2, shuffle=True)

    model_ft = models.vgg16(pretrained=True)
    n_inputs = 4096
    for param in model_ft.parameters():
        param.requires_grad = False
    model_ft.classifier[6] = nn.Sequential(
                        nn.Linear(n_inputs, 256),
                        nn.ReLU(),
                        nn.Dropout(0.5),
                        nn.Linear(256, 5),
                        )

    # model_ft = model.to(device)

    criterion = nn.CrossEntropyLoss()

    # Observe that all parameters are being optimized
    optimizer_ft = optim.SGD(model_ft.parameters(), lr=0.001, momentum=0.9)

    # Decay LR by a factor of 0.1 every 7 epochs
    exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=7, gamma=0.1)


    train_model(model_ft, criterion, optimizer_ft, exp_lr_scheduler, train_loader, val_loader, num_epochs=25)
